List prepare(text1){
  RegExp regex = RegExp("(?=[’,.?!“”()])|\\s+");
  text1 = text1.replaceAll("’", " ");
  var list = text1.split(regex).toList(); 
  
  var listFinal = <String>[];
  
  for (int i = 0; i < list.length; i++)
  {
    if (list[i].length >= 3)
    {
      listFinal.add(list[i].toLowerCase());
    }    
  }
  return listFinal;
}