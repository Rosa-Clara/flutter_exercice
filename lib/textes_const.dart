const text1 =
    "Je pense que la beauté d’une personne, n’est pas dans son physique mais bien dans sa beauté de son cœur et de son âme la beauté du cœur et de l’âme vaut mille fois plus que la beauté du corps les sentiments et la tendresse son les deux organes principaux de l’amour aimer sais regarder ensembles dans la même direction.";

const text2 = "Le désir de bien faire est un puissant moteur. Celui de faire du bien est plus puissant encore. Celui qui veut réussir trouve un moyen. Celui qui veut rien faire trouve une excuse.";

