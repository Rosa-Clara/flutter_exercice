
import 'package:exercice_appli/textes_const.dart' as etape1;
import 'package:exercice_appli/prepare.dart' as etape2;
import 'package:exercice_appli/map.dart' as etape3;

void main(List<String> arguments) {
  var text = etape1.text1;
  var list = etape2.prepare(text);
  var list_map = etape3.map(list);

  print(list_map);
}
